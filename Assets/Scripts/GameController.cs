﻿using UnityEngine;
using System.Collections;

// TODO gestion collision inter-brique

// Délimitation des positions où peuvent apparaitre les briques
[System.Serializable]
public class Spawn
{
	public Vector3 spawnBoundaryInf; 
	public Vector3 spawnBoundarySup;
}

public class GameController : MonoBehaviour 
{
	public GameObject bricks; 	// Les briques à utiliser
	public int nbBricks;		// Nombre de brique dans le niveau	
	
	public Spawn spawn; 


	// Use this for initialization
	void Start () 
	{
		// SpawnBrick();
		int nb_lines = 10; int nb_columns = 10;
		float begin_x = spawn.spawnBoundaryInf.x;
		float begin_z = spawn.spawnBoundarySup.z;

		float contours = 0.02f;

		for(int i = 0; i < nb_lines; i++) // lignes
		{
			begin_x = spawn.spawnBoundaryInf.x;
			for(int j = 0; j < nb_columns; j++)
			{
				Vector3 pos = new Vector3(begin_x, spawn.spawnBoundaryInf.y, begin_z);
				Quaternion rot = Quaternion.identity;
				Instantiate(bricks, pos, rot);

				begin_x += bricks.renderer.bounds.size.x + contours;
			}
			begin_z -= bricks.renderer.bounds.size.z + contours;
		}
	}


	void SpawnBrick()
	{
		Vector3 spawnPosition = new Vector3 (Random.Range (spawn.spawnBoundaryInf.x, spawn.spawnBoundarySup.x), 
		                                     spawn.spawnBoundaryInf.y, 
		                                     Random.Range (spawn.spawnBoundaryInf.z, spawn.spawnBoundarySup.z));
		
		Quaternion spawnRotation = Quaternion.identity;
		Instantiate (bricks, spawnPosition, spawnRotation);
	}
	

}
