﻿using UnityEngine;
using System.Collections;

public class BallController : MonoBehaviour 
{

	public float speed;
	void Start()
	{
		rigidbody.velocity = transform.forward * speed;
	}

	/**
	 *  Gestion des collisions
	 */  
	void OnTriggerEnter(Collider other)
	{
		// Si contact avec la balle, alors on détruit la brique
		if(other.tag == "Brick" || other.tag == "Brique")
		{
			Rebound(other);
			Destroy(other.gameObject);
		}
		// TODO Mettre en place le gameover :)
		else if(other.tag == "NorthWall")
		{
			Debug.Log("Game Over");
		}
		else if(other.tag == "Wall")
		{
			ReboundWall(other);
		}
		else if(other.tag == "Pad")
		{
			Rebound(other);
		}
	}


	/**
	 *	Gestion du rebond de la balle
	 */ 
	void Rebound(Collider other)
	{			
		Vector3 direction = transform.position - other.transform.position;
		direction.Normalize();
		direction *= speed; 
		speed += 0.1f;
		rigidbody.velocity = direction;
	}

	void ReboundWall(Collider other)
	{
		Vector3 dir = rigidbody.velocity;
		dir.x = -dir.x;
		rigidbody.velocity = dir;
	}
}
