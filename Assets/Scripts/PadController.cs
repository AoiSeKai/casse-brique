﻿using UnityEngine;
using System.Collections;

public class PadController : MonoBehaviour {

	public Vector3 limits_inf;
	public Vector3 limits_sup;
	public float speed;

	void FixedUpdate () 
	{
		// On ne va qu'à gauche ou à droite
		if(Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.UpArrow))
			return;
		else
		{
			// Avec le clavier
			float moveHorizontal = Input.GetAxis ("Horizontal");
			float moveVertical = Input.GetAxis ("Vertical");
			
			Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);

			rigidbody.velocity = movement * speed;
			rigidbody.position = new Vector3(
					Mathf.Clamp(rigidbody.position.x, limits_inf.x, limits_sup.x),
					0.0f,
					Mathf.Clamp(rigidbody.position.z, limits_inf.z, limits_sup.z)
			);
		}
	}
}
