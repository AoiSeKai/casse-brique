﻿using UnityEngine;
using System.Collections;

/**
 * Gestion des collisions pour les briques
 */
public class DestroyByContact : MonoBehaviour 
{

	void OnTriggerEnter(Collider other)
	{
		// Si contact avec la balle, alors on détruit la brique
		if(other.tag == "Ball")
		{
			Destroy(gameObject);
		}
	}
	
}
